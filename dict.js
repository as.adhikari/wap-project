"use strict";

var DictionaryModule = (function(){
	var searchWord = function(e){
		var word = $("#serarch_word").val();
		$.ajax({
			"url": "https://jsonplaceholder.typicode.com/posts",
			"type": "GET",
			"success": appendResult,
			"error": function(xhr, status, exception){
				console.log(xhr, status, exception);
			}
		})
    }
    
    var appendResult = function(result){

        $.each(result, function(i, item) {
            var listItem = "<li class='result-item'><span class='result-description'>" +
                item.title + 
                "</span></li>";
           $("#result-list").append(listItem);
        });
    }
	
	return {
		searchWord: searchWord
	}
})();

$(document).ready(function(){
	$("#btnLookup").on("click",DictionaryModule.searchWord);
});